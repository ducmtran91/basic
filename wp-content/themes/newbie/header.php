<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newbie
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<nav role="navigation">
			<div class="navbar navbar-static-top navbar-default">
		  <div class="container">
		    <div class="navbar-header">
		    	<?php if ( get_theme_mod( 'site_logo' ) ): ?>
				    <a class="navbar-brand logo" href="<?php echo esc_url( home_url( '/' )); ?>" title="<?php bloginfo('name') ?>" rel="homepage">
				        <img src="<?php echo esc_attr(get_theme_mod( 'site_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
				    </a>
				<?php else : ?>
					<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' )); ?>" title="<?php bloginfo('name') ?>" rel="homepage"><?php esc_url(bloginfo('name')); ?></a>
				<?php endif; ?>
			    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar-responsive-collapse">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    
			</div>
		    
		    <div class="navbar-collapse collapse" id="navbar-responsive-collapse">
		        <?php
		        wp_nav_menu( array(
		            'theme_location'    => 'primary',
		            'depth'             => 2, // chieu sau menu 2 lop cha con
		            'container'         => 'false',
		            'menu_class'        => 'nav navbar-nav',
		            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		            'walker'            => new WP_Bootstrap_Navwalker(),
		        ) );
		        ?>
		    </div>
		     </div>
		</nav>
	</header><!-- #masthead -->
