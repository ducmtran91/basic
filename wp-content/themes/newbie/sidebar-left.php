<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newbie
 */

if ( ! is_active_sidebar( 'sidebar-left' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area col-md-3 col-lg-3 col-md-pull-9" rol="complementary">
	<div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-body">
		        <?php dynamic_sidebar( 'sidebar-left' ); ?>
	        </div>
        </div>
    </div>
</aside><!-- #secondary -->
</div><!-- .row -->
</div><!-- .container index -->