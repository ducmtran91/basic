<?php
function portfolio_post_type()
{
    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label = array(
        'name' => 'Portfolio', //Tên post type dạng số nhiều
        'singular_name' => 'Portfolio' //Tên post type dạng số ít
    );
 
    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args = array(
        'labels' => $label, //Gọi các label trong biến $label ở trên
        'description' => 'Portfolio post type', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array( 'portfolio_taxonomy', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'post' //
    );
 
    register_post_type('portfolio_post_type', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
 
}

/* Kích hoạt hàm tạo custom post type */
add_action('init', 'portfolio_post_type');

function portfolio_taxonomy() {
 
        /* Biến $label chứa các tham số thiết lập tên hiển thị của Taxonomy
         */
        $labels = array(
                'name' => 'Các loại sản phẩm',
                'singular' => 'Loại sản phẩm',
                'menu_name' => 'Loại sản phẩm'
        );
 
        /* Biến $args khai báo các tham số trong custom taxonomy cần tạo
         */
        $args = array(
                'labels'                     => $labels,
                'hierarchical'               => false,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
        );
 
        /* Hàm register_taxonomy để khởi tạo taxonomy
         */
        register_taxonomy('portfolio_taxonomy', 'post', $args);
 
}
 
// Hook into the 'init' action
add_action( 'init', 'portfolio_taxonomy', 0 );

/**
* Displaying Custom Post Types on The Front Page
*/
add_action( 'pre_get_posts', 'add_frontend_post_types' );
 
function add_frontend_post_types( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set( 'post_type', array( 'portfolio_post_type' ) );
    return $query;
}

// Creating the Custom Field Box
add_action("admin_init", "portfolio_meta_box");
 
function portfolio_meta_box() {
    add_meta_box("projInfo-meta", "Project Options", "portfolio_meta_options", "portfolio_post_type", "side", "low");
}
   
 
function portfolio_meta_options() {
        global $post;
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
        $link = $custom["projLink"][0];
?>  
    <label>Link:</label><input name="projLink" value="<?php echo $link; ?>" />
<?php
    }
// Saving the Custom Data
add_action('save_post', 'save_project_link');
   
function save_project_link() {
    global $post;
    
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return $post_id;
    } else {
        update_post_meta($post->ID, "projLink", $_POST["projLink"]);
    }
}

//Customizing Admin Columns
add_filter("manage_edit-portfolio_post_type_columns", "project_edit_columns");
   
function project_edit_columns($columns){
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => "Project",
            "description" => "Description",
            "link" => "Link",
            "type" => "Type of Project",
        );
   
        return $columns;
}
 
add_action("manage_posts_custom_column",  "project_custom_columns");
   
function project_custom_columns($column) {
        global $post;
        switch ($column) 
        {
            case "description":
                the_excerpt();
                break;
            case "link":
                $custom = get_post_custom();
                echo $custom["projLink"][0];
                break;  
            case "type":
                echo get_the_term_list($post->ID, 'portfolio_taxonomy', '', ', ','');
                break;
        }
}

//Adding Some Display Functions
add_filter('excerpt_length', 'my_excerpt_length');
 
function my_excerpt_length($length) {
 
    return 25;
 
}

add_filter('excerpt_more', 'new_excerpt_more');
 
function new_excerpt_more($text) {
 
    return ' ';
 
}
 
function portfolio_thumbnail_url($pid){
    $image_id = get_post_thumbnail_id($pid);
    $image_url = wp_get_attachment_image_src($image_id,'screen-shot');
    return  $image_url[0];
}