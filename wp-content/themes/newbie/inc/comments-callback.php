<?php
/**
* Template for comments and pingbacks.
*
* Used as a callback by wp_list_comments() for displaying the comments.
*
*/
if(!function_exists('newbie_comment')) {
    // My custom comments output html
	function newbie_comment( $comment, $args, $depth ) {

		$GLOBALS['comment'] = $comment;

		// Switch between different comment types
		switch ( $comment->comment_type ) :
			case 'pingback' :
			case 'trackback' : 
		?>
		<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
			<p>
				<?php _e('Pingback:', 'newbie'); ?>
				<?php comment_author_link(); ?>
				<?php edit_comment_link(__('Edit', 'newbie'), '<span class="ping-meta"><span class="edit-link">', '</span></span>'); ?>
			</p>
		<?php
				break;
			default :
		?>
		<li id="li-comment-<?php comment_ID() ?>">
			<article id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
				
				<div class="comment-author vcard pull-left">
					<?php
					// Display avatar unless size is set to 0
					if ( $args['avatar_size'] != 0 ) {
						$avatar_size = ! empty( $args['avatar_size'] ) ? $args['avatar_size'] : 70; // set default avatar size
							echo get_avatar( $comment, $avatar_size );
					}
					// Display author name
					//printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>', 'textdomain' ), get_comment_author_link() ); ?>
				</div><!-- .comment-author -->
				
				<div class="comment-details">
					
					<header class="comment-meta">
						<cite class="fn">
							<?php comment_author_link(); ?>
						</cite>
						</br>
						<div class="comment-date">
							<?php
								printf('<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
									esc_url(get_comment_link($comment->comment_ID)),
									get_comment_time('c'),
									sprintf(_x('%1$s at %2$s', '1:date, 2:time', 'newbie'), get_comment_date(), get_comment_time())
								);
								edit_comment_link( __( 'Edit', 'newbie' ), '<span class="edit-link">', '</span>' );
							?>
						</div>
					</header><!-- .comment-meta -->

					
					<?php if ( $comment->comment_approved == '0' ) { ?>
						<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'newbie' ); ?></p>
					<?php } ?>

					<div class="comment-content">
						<?php comment_text(); ?>
					</div><!-- .comment-content -->

					<div class="reply">
						<?php
							// Display comment reply link
							comment_reply_link( array_merge( $args, array(
								'reply_text' => __('reply to comment', 'newbie').' &rarr;',
								'depth'     => $depth,
								'max_depth' => $args['max_depth']
							) ) ); 
						?>
					</div>
				</div><!-- .comment-details -->
			</article><!-- #comment-## -->
		<?php
			break;
		endswitch; // End comment_type check.
	}
}