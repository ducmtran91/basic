<?php
/**
 * newbie Theme Customizer
 *
 * @package newbie
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function newbie_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'newbie_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'newbie_customize_partial_blogdescription',
			)
		);

		// show menu section in customizer
		$wp_customize->add_section('site_logo_section', array(
			'title' => __('Logo', 'newbie'),
			'priority' => 30,
			'description' => 'Upload a logo to replace the default site name and description in the header'
		));

		//Settings
	    $wp_customize->add_setting( 'site_logo' );

	    //Controls
	    $wp_customize->add_control(
	        new WP_Customize_Image_Control(
	            $wp_customize,
	            'site_logo',
	            array(
	                'label'      => 'Upload a logo',
	                'section'    => 'site_logo_section',
	                'settings'   => 'site_logo'
	            )
	        )
	    );
	}


}
add_action( 'customize_register', 'newbie_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function newbie_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function newbie_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function newbie_customize_preview_js() {
	wp_enqueue_script( 'newbie-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'newbie_customize_preview_js' );