<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newbie
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<?php if(has_nav_menu('footer-menu', 'newbie')) { ?>
						<nav role="navigation">
							<?php 
								wp_nav_menu(array(
									'container' 	=> '',
									'menu_class' 	=> 'footer-menu',
									'theme_location'=> 'footer-menu'
								));
							?>
						</nav>
					<?php } ?>
				</div>

				<div class="col-md-6 col-lg-6">
					<p class="alignright">&copy; <?php _e('Copyright', 'newbie'); ?> <?php echo date("Y"); ?> - <a href="<?php echo home_url(); ?>/" title="<?php bloginfo('name'); ?>" role="home"><?php bloginfo('name'); ?></a> </p>
				</div>
			</div><!-- .row -->
		</div><!-- .container index -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
