﻿<?php
/**
 * Template Name: Homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package newbie
 */

get_header();
?>
    <div id="content">
        <div class="container">
    	    <div class="row">
    			<div class="jumbotron">
                    <h1>Navbar example</h1>
                    <p>This example is a quick exercise to illustrate how the default, static navbar and fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts to your viewport and device.</p>
                    <p>
                        <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs »</a>
                    </p>
                </div>
    		</div>
    	</div>
    </div>
<?php
get_footer();
