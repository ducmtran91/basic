<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package newbie
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				newbie_posted_on();
				newbie_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php newbie_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'newbie' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'newbie' ),
				'after'  => '</div>',
			)
		);
		?>

		<?php 
		// Display author bio if post isn't password protected
		if(! post_password_required()) : 
		?>
			<?php if(get_the_author_meta('description') != '') : ?>
				<div class="author-meta well well-lg">
					<div class="media">
						<div class="media-object pull-left">
							<?php 
								if(function_exists('get_avatar')) {
									echo get_avatar(get_the_author_meta('ID'), 80);
								} 
							?>
						</div>

						<div class="media-body">
							<h5 class="media-heading"><?php the_author_posts_link(); ?></h5>
							<p><?php the_author_meta('description') ?></p>
							<?php
								$twitterHandle = get_the_author_meta('twitter');
								$facebookHandle = get_the_author_meta('facebook');
								$gplusHandle = get_the_author_meta('gplus');
							?>
							<?php if($twitterHandle != ''): ?>
								<a href="<?php echo esc_html($twitterHandle); ?>" targe="blank"><i class="fa fa-twitter"></i></a>
							<?php endif; ?>
							<?php if($facebookHandle != ''): ?>
								<a href="<?php echo esc_html($facebookHandle); ?>" targe="blank"><i class="fa fa-facebook"></i></a>
							<?php endif; ?>
							<?php if($gplusHandle != ''): ?>
								<a href="<?php echo esc_html($gplusHandle); ?>" targe="blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		<?php endif; ?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php newbie_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
