<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package newbie
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title">
			<?php
			$newbie_comment_count = get_comments_number();
			if ( '1' === $newbie_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'newbie' ),
					'<span>' . wp_kses_post( get_the_title() ) . '</span>'
				);
			} else {
				printf( 
					/* translators: 1: comment count number, 2: title. */
					esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $newbie_comment_count, 'comments title', 'newbie' ) ),
					number_format_i18n( $newbie_comment_count ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					'<span>' . wp_kses_post( get_the_title() ) . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
					'callback'   => 'newbie_comment',
					'avatar_size'=> 80
				)
			);
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'newbie' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	$req = get_option('required_name_email');
	$aria_req = ($req ? " aria-required='true'" : '');

	$comments_args = array(
		'label_submit' => 'Submit',
		'title_reply' => 'Leave a Comment',
		'comment-notes-after' => '',
		'comment-field' => '<div class="form-group"><label for="comment">'._x('Comment', 'newbie').'</label><textarea class="form-control" rows="10" id="comment" name="comment" aria-required="true"></textarea></div>',
		'fields' => apply_filters('comment_form_default_fields', array(
			'author' => '<div class="form-group">'.
				'<label for="author">'.__('Name', 'newbie').'</label>'.
				($req ? '<span class="required"></span>' : '') .
				'<input class="form-control" id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'" size="30"'. $aria_req.'/></div>',
			'email' => '<div class="form-group"> <label for="email">'.__('Email', 'newbie').'</label>'.
				($req ? '<span class="required"></span>' : '') .
				'<input class="form-control" id="email" name="email" type="text" value="'.esc_attr($commenter['comment_author_email']).'" size="30"'. $aria_req.'/></div>',
			'url' => '<div class="form-group"> <label for="url">'.__('Website', 'newbie').'</label>'.
				($req ? '<span class="required"></span>' : '') .
				'<input class="form-control" id="url" name="url" type="text" value="'.esc_attr($commenter['comment_author_url']).'" size="30"'. $aria_req.'/></div>',
			)
		)
	);

	comment_form($comments_args);
	?>

</div><!-- #comments -->
