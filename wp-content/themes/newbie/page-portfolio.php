<?php
/* Template Name: Portfolio */
add_filter('query_vars', 'portfolio_parameter_queryvars' );
function portfolio_parameter_queryvars( $qvars )
{
	$qvars[] = 'term';
	return $qvars;
}
 
get_header(); 

$terms = get_terms( array(
		    'taxonomy' => 'portfolio_taxonomy',
		    'hide_empty' => false,
		) );

wp_reset_query();

$term_slug = empty($terms) ? '' : $terms[0]->slug;

if(isset($wp_query->query_vars['term'])) {
	$term_slug = urldecode($wp_query->query_vars['term']);
}

$the_query = new WP_Query( array(
    'post_type' => 'portfolio_post_type',
    'tax_query' => array(
        array (
            'taxonomy' => 'portfolio_taxonomy',
            'field' => 'slug',
            'terms' => $term_slug,
        )
    ),
) );
 
?>

<div class="container">
	<div class="row">
	    <div id="primary">
	        <main id="main" class="site-main" role="main">
				<div id="portfolio" class="portfolio pt-2"> 
				 
					<h2>Portfolio of Work</h2>
					 
					<?php
							if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
								$page_url = add_query_arg(array(), get_permalink());
							    echo '<ul id="portfoliotabs">';
							    foreach ( $terms as $term ) {
							    	$active = '';
							    	if($term->slug == $term_slug) {
							    		$active = "class='active'";
							    	}
							        echo '<li><a title="'.$term->name.'" '.$active.' href="'.$page_url.'/'.$term->slug.'">' . $term->name . '</a></li>';
							    }
							    echo '</ul>';
							}
						?>

					<div class="row flex">

						
					 
						<?php if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>
						 
						    <?php
						        $title= str_ireplace('"', '', trim(get_the_title()));
						        $desc= str_ireplace('"', '', trim(get_the_content()));
						    ?>   
						 
						    <div class="col-sm-6 col-md-4">
				                <div class="thumbnail">
				                	<a title="<?=$title?>: <?=$desc?>" rel="lightbox[work]" href="<?php print  portfolio_thumbnail_url($post->ID) ?>">
				                		<?php the_post_thumbnail(); ?>
			                		</a>
			                	
					                <div class="caption">
					                	<h3 class="mb-1" title="<?php echo $title ?>">
					                		<a href="<?php the_permalink($post->ID); ?>">
					                			<?php echo $title ?>
					                		</a>
				                		</h3>
					                	<p class="mb-1">
					                		<?php print get_the_excerpt(); ?>
					                		<a title="<?=$title?>: <?=$desc?>" rel="lightbox[work]" href="<?php print  portfolio_thumbnail_url($post->ID) ?>">(more)</a>
					                	</p>
						                <?php $site= get_post_custom_values('projLink'); 
						                    if($site[0] != ""){ ?>
						                    <p><a href="<?=$site[0]?>">Visit the Site</a></p>
						                <?php }else{ ?>
						                    <p><em>Live Link Unavailable</em></p>
						                <?php } ?>
					                </div>
				                </div>
				            </div>
						 
						<?php endwhile; endif; ?>
					 
					</div>
				 
				</div>
			</main><!-- end main -->
        </div><!-- #primary -->
 	</div>
 </div>
<?php 
	get_footer();
?>