<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_newbie' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`u%;b}`jm*vm`g3S!U.mXdy,_BDvEpOg~V![a| u(s4Dx4,O|~,p$ti5Lyh;j+QZ' );
define( 'SECURE_AUTH_KEY',  'rQ[Da{,HyLU>jJ~dyndRl99d1 r=({-qZ1]xl%70_oIiA(%F0]M,b&3Kn#_V=X8O' );
define( 'LOGGED_IN_KEY',    'L!fe|a0!Y;ys5p6xr1e0Q&t<7/k,iS`5S<_vQ4G {/+7Xr}1/DikID`P0&?qeM%d' );
define( 'NONCE_KEY',        '`33R-sO5i%Dvl#wwKLD8OozT#vALsm=R*KAW_|%.omsdNpTify:N^M2_~d0{w=} ' );
define( 'AUTH_SALT',        'f>Qw@i]=UwE{/s4xE;!`Xk!r0DNm1i,JDJ)8$SWb!b<8GP0fdXI|p!$rU/J.T{U/' );
define( 'SECURE_AUTH_SALT', '~o86)^cGZrA[PAt*X-]R9)[kp_J_]dL_^?kco1MT_jAI-S$LB@~ohnD}]ytG7;7e' );
define( 'LOGGED_IN_SALT',   '+!hF&}(]]vB_sGmW-L>Am5%`(`5)R&_#Z7 UK)]aG@4``]%~1krZKmzluIcrZ2BI' );
define( 'NONCE_SALT',       'GGb]Sj-]@h}TLJ[$54y![?x){~}):b-zcVQh~s$E6)lF?3Vw7[6|C7}NG3IuCeuU' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
